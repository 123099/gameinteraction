﻿using System;
using UnityEngine;

namespace GameInteraction
{
    [RequireComponent(typeof(SphereCollider), typeof(AudioSource))]
    public abstract class Pickup : ValueInteractable
    {
        protected override void OnTriggerEnter(Collider collider)
        {
            base.OnTriggerEnter(collider);
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            float killTime = audio.clip.length;
            foreach (Renderer r in GetComponentsInChildren<Renderer>())
                r.enabled = false;
            Destroy(gameObject, killTime);
        }
    }
}
