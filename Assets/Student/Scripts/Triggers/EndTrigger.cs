﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag("Ball"))
        {
            Debug.Log("Player has won! :D");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
