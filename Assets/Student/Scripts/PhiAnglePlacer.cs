﻿using UnityEngine;
using System.Collections;

public class PhiAnglePlacer : MonoBehaviour {

    [SerializeField]
    private Transform firstObject;
    [SerializeField]
    private float radius;
    [SerializeField]
    private int count;
    [SerializeField]
    private float randomZDeviation = 0.2f;

    private float cX;
    private float cY;

	// Use this for initialization
	void Start () {
        cX = firstObject.position.x;
        cY = firstObject.position.y - radius;
        Random.seed = 19876549;
        for (int i = 0; i < count; i++)
        {
            GameObject phiObject;
            if (i == 0)
                phiObject = firstObject.gameObject;
            else
                phiObject = Instantiate(firstObject.gameObject);
            phiObject.name = "Phi " + i;
            float newX = cX + radius * Mathf.Cos(Mathf.Deg2Rad * 137.5f * i);
            float newY = cY - radius * Mathf.Sin(Mathf.Deg2Rad * 137.5f * i);
            float randomZ = Random.Range(-randomZDeviation, randomZDeviation);
            phiObject.transform.position = new Vector3(newX, newY, firstObject.position.z + randomZ);
        }
	}
}
