﻿using UnityEngine;
using System.Collections;
using GameInteraction;

public class Magnet : MonoBehaviour
{
    void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Ball>())
        {
            foreach (Transform t in transform)
            {
                MoveTo m;
                if (m = t.GetComponent<MoveTo>())
                    m.SetTarget(c.transform);
            }
        }
    }
}
