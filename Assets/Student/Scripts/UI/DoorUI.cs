﻿using UnityEngine;
using System.Collections;
using GameInteraction;
using UnityEngine.UI;

[RequireComponent(typeof(CoinDoorTrigger))]
public class DoorUI : MonoBehaviour {

    private CoinDoorTrigger cdt;
    [SerializeField]
    private Text amountText;
    [SerializeField]
    private Ball ball;


	// Use this for initialization
	void Start () {
        cdt = GetComponent<CoinDoorTrigger>();
	}

    void updateUI()
    {
        amountText.text = ball.score + "/" + cdt.value;
    }

    void Update()
    {
        updateUI();
    }
}
