﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour {

    private bool paused = false;
    private Text btnText;

    void Start()
    {
        btnText = GetComponentInChildren<Text>();
    }

    public void OnClick()
    {
        paused = !paused;
        togglePause();
    }

    void togglePause()
    {
        if (paused)
        {
            Time.timeScale = 0;
            btnText.text = "|>";
        }
        else {
            Time.timeScale = 1;
            btnText.text = "| |";
        }
    }
}
