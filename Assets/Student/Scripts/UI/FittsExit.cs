﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FittsExit : MonoBehaviour {

    [SerializeField]
    private Button[] buttons;
    [SerializeField]
    private Vector2 growToSize;

    private Vector2 origSize;
    private Color origColor;

	// Use this for initialization
	void Start () {
        toggleButtonState(false);
        Vector2 size = GetComponent<RectTransform>().sizeDelta;
        origSize = new Vector2(size.x, size.y);
        Color c = GetComponent<Image>().color;
        origColor = new Color(c.r, c.g, c.b, c.a);
	}
	
	public void OnMouseEnter()
    {
        toggleButtonState(true);
        GetComponent<RectTransform>().sizeDelta = growToSize;
        GetComponent<Image>().color = new Color(origColor.r, origColor.g, origColor.b, 0);
    }

    public void OnMouseExit()
    {
        toggleButtonState(false);
        GetComponent<RectTransform>().sizeDelta = origSize;
        GetComponent<Image>().color = origColor;
    }

    void toggleButtonState(bool active)
    {
        foreach (Button b in buttons)
            b.gameObject.SetActive(active);
    }
}
