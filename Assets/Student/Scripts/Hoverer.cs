﻿using UnityEngine;
using System.Collections;

public class Hoverer : MonoBehaviour {

    [SerializeField]
    [Range(0.5f, 5f)]
    private float distance;
    [SerializeField]
    [Range(0, 100)]
    private float speed;

    private float distMoved;
    private int direction;
    // Use this for initialization
    void Start()
    {
        distMoved = 0;
        direction = 1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, direction * speed * Time.deltaTime, 0, Space.World);
        distMoved += speed * Time.deltaTime;

        if (distMoved >= distance)
        {
            distMoved = 0;
            direction *= -1;
        }
    }
}
