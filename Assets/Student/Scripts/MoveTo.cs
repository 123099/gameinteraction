﻿using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour {

    [SerializeField]
    private float moveSpeed = 0.1f;
    [SerializeField]
    private bool destroy = true;

    private Transform target;

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, moveSpeed);
            if (destroy && Vector3.Distance(transform.position, target.position) < 0.1f)
                Destroy(gameObject);
        }
    }

    public void SetTarget(Transform t)
    {
        target = t;
    }
}
