﻿using UnityEngine;
using System.Collections;

public class Quit : MonoBehaviour {

    void OnTriggerEnter()
    {
        Application.Quit();
    }
}
