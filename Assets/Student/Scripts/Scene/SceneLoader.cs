﻿using GameInteraction;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    [SerializeField]
    private int sceneIndex = 0;

	public void LoadScene(int index)
    {
        SceneManager.LoadScene(index); 
    }

    void OnTriggerEnter(Collider c)
    {
        if(c.GetComponent<Ball>())
            LoadScene(sceneIndex);
    }
}
