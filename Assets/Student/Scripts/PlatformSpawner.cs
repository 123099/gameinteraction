﻿using UnityEngine;
using System.Collections;
using GameInteraction;

public class PlatformSpawner : MonoBehaviour {

    [SerializeField]
    private GameObject[] targets;

	// Use this for initialization
	void Start () {
        if (targets == null)
            return;

        foreach (GameObject t in targets)
            t.SetActive(false);
	}
	
	void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Ball>())
        {
            foreach (GameObject t in targets)
                t.SetActive(true);
        } 
    }
}
