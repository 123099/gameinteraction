﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GameInteraction;

public class CoinUI : MonoBehaviour {

    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private Sprite image;

    public void AddImage(int currentScore)
    {
        GameObject obj = new GameObject("Image");
        obj.AddComponent<Image>();
        Image image = obj.GetComponent<Image>();
        image.sprite = this.image;
        image.transform.SetParent(canvas.transform);
        RectTransform rt = image.rectTransform;
        rt.sizeDelta = new Vector3(100, 50);
        //rt.localPosition = new Vector2(currentScore * 105 - canvas.pixelRect.width/2, canvas.pixelRect.height/5);
        rt.anchorMax = new Vector2(0.056f, 1-0.147f);
        rt.anchorMin = new Vector2(0.056f, 1 - 0.147f);
        rt.anchoredPosition = new Vector2(currentScore * 105, 0);
    }
}
