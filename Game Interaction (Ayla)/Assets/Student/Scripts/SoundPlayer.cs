﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

    [SerializeField]
    private AudioClip enterSound;
    [SerializeField]
    private float enterDelay;
    [SerializeField]
    private AudioClip staySound;
    [SerializeField]
    private float stayDelay;
    [SerializeField]
    private bool stayLoop;
    [SerializeField]
    private AudioClip exitSound;
    [SerializeField]
    private float exitDelay;
    [SerializeField]
    private bool listenToCollisions = true;
    [SerializeField]
    private AudioSource source;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    //Triggers
    void OnTriggerEnter()
    {
        if(listenToCollisions)
            playEnter();
    }

    void OnTriggerStay()
    {
        if (listenToCollisions)
            playStay();
    }

    void OnTriggerExit()
    {
        if (listenToCollisions)
            playExit();
    }

    //Collisions
    void OnCollisionEnter()
    {
        if (listenToCollisions)
            playEnter();
    }

    void OnCollisionStay()
    {
        if (listenToCollisions)
            playStay();
    }

    void OnCollisionExit()
    {
        if (listenToCollisions)
            playExit();
    }

    public void playEnter()
    {
        if (enterSound == null) return;
        source.clip = enterSound;
        source.loop = false;
        source.PlayDelayed(enterDelay);
    }

    public void playStay()
    {
        if (staySound == null || source.clip == staySound) return;
        source.clip = staySound;
        source.loop = stayLoop;
        source.PlayDelayed(stayDelay);
    }

    public void playExit()
    {
        if (exitSound == null) return;
        source.clip = exitSound;
        source.loop = false;
        source.PlayDelayed(exitDelay);
    }
}
