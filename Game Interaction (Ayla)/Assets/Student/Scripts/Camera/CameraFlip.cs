﻿using UnityEngine;
using System.Collections;
using GameInteraction;

public class CameraFlip : MonoBehaviour {

    [SerializeField]
    private bool onDoorOpen = false;

    [SerializeField]
    private float angle = 180f;
    private float currentAngle = 0;

    [SerializeField]
    private bool remainRotated;

    private Camera cam;

    void Start()
    {
        cam = Camera.main;
    }

    void OnTriggerEnter(Collider c)
    {
        currentAngle = cam.transform.rotation.eulerAngles.z;
        flipCamera(c,angle);
    }

    void OnTriggerExit(Collider c)
    {
        Debug.Log(currentAngle);
        if (remainRotated)
            return;

        flipCamera(c,currentAngle);
    }

    private void flipCamera(Collider c, float angle)
    {
        if ((onDoorOpen && GetComponent<CoinDoorTrigger>().isOpen || !onDoorOpen) && c.gameObject.CompareTag("Ball"))
        {
            SmoothFollow smoothFollow;
            if (smoothFollow = cam.GetComponent<SmoothFollow>())
            {
                smoothFollow.SetAngle(angle);
            }
        }
    }
}
