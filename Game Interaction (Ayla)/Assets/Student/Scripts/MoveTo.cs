﻿using UnityEngine;
using System.Collections;

public class MoveTo : MonoBehaviour {

    [SerializeField]
    private float moveSpeed = 0.1f;

    private Transform target;

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, moveSpeed);
        }
    }

    public void SetTarget(Transform t)
    {
        target = t;
    }
}
