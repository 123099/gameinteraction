﻿using UnityEngine;
using System.Collections;
using GameInteraction;

[RequireComponent(typeof(Ball))]
public class BallHealth : MonoBehaviour {

    [SerializeField]
    private Color fullHealthColor;
    [SerializeField]
    private Color mediumHealthColor;
    [SerializeField]
    private Color lowHealthColor;

    [SerializeField]
    private float fullHealth;
    [SerializeField]
    private float mediumHealth;
    [SerializeField]
    private float lowHealth;

    private Ball ball;
    private Material ballMat;

    // Use this for initialization
    void Start () {
        ball = GetComponent<Ball>();
        ballMat = GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        float health = (float)ball.health/ball.MaxHealth;
        if (health >= fullHealth)
        {
            ballMat.color = fullHealthColor;
        }
        else if (health >= mediumHealth)
        {
            ballMat.color = mediumHealthColor;
        }
        else if(health >= lowHealth)
        {
            ballMat.color = lowHealthColor;
        }
	}
}
