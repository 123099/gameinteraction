﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

    [SerializeField]
    private float x = 50f;
    [SerializeField]
    private float y = 50f;
    [SerializeField]
    private float z = 50f;
    // Update is called once per frame
    void Update () {

        transform.Rotate(new Vector3(x, y, z) * Time.deltaTime);	
	}
}
