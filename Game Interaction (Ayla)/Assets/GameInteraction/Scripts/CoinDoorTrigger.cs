﻿using UnityEngine;

namespace GameInteraction
{
    public class CoinDoorTrigger : ValueInteractable
    {
        [SerializeField]
        new private Animation animation;
        [SerializeField]
        private bool stayOpen = true;

        private bool _isOpen = false;
        public bool isOpen{ get { return _isOpen; } protected set { _isOpen = value; } }

        protected override void OnInteract(Ball ball)
        {
            if (ball.score >= value && !isOpen && animation)
            {
                animation["OpenDoor"].speed = 1;
                animation.Play();
                SoundPlayer sp = GetComponent<SoundPlayer>();
                if (sp)
                    sp.playEnter();
                isOpen = true;
            }
        }

        void OnTriggerExit(Collider c)
        {
            if (stayOpen) return;

            Ball ball = c.GetComponent<Ball>();
            if (ball)
            {
                animation["OpenDoor"].speed = -1;
                if(animation["OpenDoor"].time == 0)
                    animation["OpenDoor"].time = animation["OpenDoor"].length;
                animation.Play();
                SoundPlayer sp = GetComponent<SoundPlayer>();
                if (sp)
                    sp.playExit();
                isOpen = false;
            }
        }
    }
}
