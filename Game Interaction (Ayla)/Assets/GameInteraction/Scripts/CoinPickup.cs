﻿using UnityEngine;

namespace GameInteraction
{
    public class CoinPickup : Pickup
    {
        protected override void OnInteract(Ball ball)
        {
            GetComponent<CoinUI>().AddImage(ball.score);
            ball.AddScore(value);
        }
    }
}
